""" This script extract the potelets incidents from the FixMyStreet website (from all subcategory: missing and damaged)
    By using the FixMyStreet API
    And put them in a sqlite database
    If the database file already exist, it looks for the last updatedDate in the db and request only the incidents
    That have been updated after this date to the FixMyStreet API
"""

# PROBLEMS/TODO:
# - ACTORS where not put into a separate tables as it was to tricky to make sure they were right
# ex: we get to a story with a departement that was not yet added in the db
# then we actually have the information for the name but it will not be writen
# because we do not have an id to reference
# this could be avoided by runing all the incidents and attachment, then the story?

import os
import time

from utils.fixmystreetExtraction import (
    requestAmountOfIncidents,
    requestTotalNumberOfPotelets,
    requestPotelets,
    requestAttachments,
    requestHistory
)
from utils.dbInteraction import (
    createConnection,
    initPoteletsDB,
    getLastUpdate,
    addAllPotelets
)

DB_PATH = '../data/potelets.db'
SHOW_PRINT_DETAILS = False


if __name__ == '__main__':

    print('_¡_¡_ POTELETS - EXTRACTION _¡_¡_')

    # --- STATS
    total_number_of_incident = requestAmountOfIncidents()[0]
    print('Total number of incidents: ' + str(total_number_of_incident))

    total_number_of_potelet = requestTotalNumberOfPotelets()
    print('Total number of potelets: ' + str(total_number_of_potelet))
    print('')

    # --- INITIALIZE DB
    print('0. Checking if db exist or not',  + DB_PATH)
    start_time = time.time()
    action = ""

    # check if it exist
    if os.path.isfile(DB_PATH):
        print("-- UPDATING DB")
        action = "updating"

    else:
        print("-- CREATING NEW DB")
        with open(DB_PATH, 'w') as file:
            pass
        print('-- new db file created')
        action = "creating"
    print('')

    # create connection
    print('1. Establishing db connection')
    conn = createConnection(DB_PATH)
    if action == "creating":
        print("creating tables ")
        initPoteletsDB(conn)
        since = originDate

    elif action == "updating":
        since = getLastUpdate(conn)
        print("-- db was last updated the " + since)
    print('')

    # to test
    # since = "2021-01-01"

    # --- EXTRACT FROM API
    print('2. Requesting from FixMyStreet API')
    print('request all potelets since ' + since)
    print('')
    potelets = requestPotelets(since)
    print('-- found '+ str(len(potelets)) +' new/updated potelets incidents')
    print('')
    print('request their attachments and history')
    i = 1
    for potelet in potelets:
        print('potelet att/hist: ' + str(i) +'/'+ str(len(potelets)) + ' ...')
        potelet['attachments'] = requestAttachments(potelet)
        potelet['history'] = requestHistory(potelet)
        i += 1
    print('')

    # --- WRITE IN THE DB
    print('3. Writing data into the sqlite database')
    print('')
    addAllPotelets(conn, potelets, SHOW_PRINT_DETAILS)
    print('')
    conn.close()

    # printing the time it tooks
    print('4. Printing times')
    duration = time.time() - start_time
    ratio = duration / len(potelets)
    total_duration = ratio * total_number_of_potelet
    print('process finished in: ' + time.strftime('%H:%M:%S', time.gmtime(duration)))
    print('rate of ' + str(ratio) + ' seconds per potelets')
    print('estimated time for all potelets: ' + time.strftime('%H:%M:%S', time.gmtime(total_duration)))
