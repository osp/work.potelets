
from jinja2 import Template

from utils.dbInteraction import (
    createConnection,
    getPotelets,
    getClusters,
    getAttachments,
    getHistory,
    getLastUpdate
)

from utils.formatingUtils import (
    municipalityFilter,
    formatingCluster,
    formatingPotelet,
    format_date,
    addGeojsonToClusters
)

MUNICIPALITIES = {
    1000: "Bruxelles-Ville",
    1020: "Laeken",
    1030: "Schaerbeek",
    1040: "Etterbeek",
    1050: "Ixelles",
    1060: "Saint-Gilles",
    1070: "Anderlecht",
    1090: "Jette",
    1080: "Molenbeek-Saint-Jean",
    1081: "Koekelberg",
    1082: "Berchem-Sainte-Agathe",
    1083: "Ganshoren",
    1140: "Evere",
    1150: "Woluwe-Saint-Pierre",
    1160: "Auderghem",
    1170: "Watermael-Boitsfort",
    1180: "Uccle",
    1190: "Forest",
    1200: "Woluwe-Saint-Lambert",
    1210: "Saint-Josse-ten-Noode"
}

# number of pages:
# 35+38+24+305+108+13+163+36+259+155+8+139+297+202+17+464+162+1+12+22

CLUSTER_THRESHOLD = 4
CODE = 1210

DB_PATH = '../data/potelets.db'
GEOJSON_PATH = '../data/potelets_clustered.geojson'
TEMPLATE_PATH = '../layout/template/template.html'
HTML_PATH = '../layout/potelets.html'


if __name__ == '__main__':

    print('___ Writing the HTML layout ___')


    print('1. Getting back the potelets from the db ' + DB_PATH)
    conn = createConnection(DB_PATH)
    # potelets = getPotelets(conn, 100)
    clusters = getClusters(conn, CLUSTER_THRESHOLD)
    print("-- Data contains", len(clusters), "clusters with more than", CLUSTER_THRESHOLD, "incidents")

    date = format_date(getLastUpdate(conn))
    volume = {
        "name": MUNICIPALITIES[CODE],
        "code": CODE,
        "threshold": CLUSTER_THRESHOLD,
        "date": date
    }

    print('2. Filtering Clusters by municipalities')
    clusters = municipalityFilter(clusters, CODE)
    volume["cluster_size"] = len(clusters)
    print("--", volume["cluster_size"], "are in the municipality:", CODE)

    print('3. Formating the potelets data')
    t = 0
    for cluster in clusters:
        cluster = formatingCluster(cluster)
        for incident in cluster["incidents"]:
            # we merge the tables in order to have one dict like object by potelet
            incident["attachments"] = getAttachments(conn, incident)
            incident["history"] = getHistory(conn, incident)
            incident = formatingPotelet(incident)
        t += len(cluster["incidents"])
    volume["incident_size"] = t
    print("-- Total number of", volume["incident_size"], "incidents selected")

    print('4. Opening template file')
    with open(TEMPLATE_PATH, 'r') as file:
        template = Template(file.read())

    print('5. Adding geojson data')
    clusters = addGeojsonToClusters(clusters)

    print('6. Printing HTML layout in ' + HTML_PATH)
    html = template.render(clusters = clusters, volume = volume)
    with open(HTML_PATH, 'w') as file:
        file.write(html)

    print('-- Layout generated')
