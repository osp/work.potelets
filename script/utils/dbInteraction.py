
import json
import sqlite3
from sqlite3 import Error
from datetime import datetime

# ___ DB WRITING ___

def createConnection(db_file):
    """ create a database connection to a database that resides
        in the memory
    """
    conn = None;
    try:
        conn = sqlite3.connect(db_file)
        conn.row_factory = sqlite3.Row
        return conn
    except Error as e:
        print(e)


# ___ CREATE THE DB ___

def createTable(conn, table_sql):
    """ create a table from the table_sql statement
    """
    try:
        c = conn.cursor()
        c.execute(table_sql)
    except Error as e:
        print(e)

def initPoteletsDB(conn):
    """ create the tables for the potelets db
    """
    potelets_table = """ CREATE TABLE IF NOT EXISTS potelets (
                         id integer PRIMARY KEY,
                         status text,
                         subcat text,
                         adress text,
                         coordinates text,
                         geo_coordinates text,
                         creationDate text,
                         updatedDate text,
                         duplicates text,
                         severalOccurrence text,
                         responsibleOrganisation text,
                         responsibleDepartment text,
                         cluster_id integer,
                        ); """

    attachments_table = """ CREATE TABLE IF NOT EXISTS attachments (
                         id integer PRIMARY KEY,
                         potelet_id integer,
                         date text,
                         type text,
                         text text,
                         href text,
                         actor_type text,
                         organisation text,
                         department text
                        ); """

    history_table = """ CREATE TABLE IF NOT EXISTS history (
                         id integer PRIMARY KEY,
                         potelet_id integer,
                         date text,
                         type text,
                         actor_type text,
                         organisation text,
                         department text
                        ); """

    create_table(conn, potelets_table);
    create_table(conn, attachments_table);
    create_table(conn, history_table);


# ___ GET ENTRIES ___

def getPotelets(conn, limit=0):
    """ return all the potelet cases from the sqlite db
    """
    cur = conn.cursor()
    if limit == 0:
        sql = ''' SELECT * FROM potelets ORDER BY id'''
        cur.execute(sql)
    else:
        sql = ''' SELECT * FROM potelets ORDER BY id DESC LIMIT (?)'''
        cur.execute(sql, [limit])

    # convert from a list of sqlite3.Row objects to real dict objects
    potelets = [dict(potelet) for potelet in cur.fetchall()]

    return potelets

def getAttachments(conn, potelet):
    """ return the attachments of a potelet by doing a sql request
    """
    values = [potelet["id"]]
    sql = ''' SELECT * FROM attachments WHERE potelet_id=(?) ORDER BY date ASC'''
    cur = conn.cursor()
    cur.execute(sql, values)
    attachments = [dict(attachment) for attachment in cur.fetchall()]

    return attachments

def getHistory(conn, potelet):
    """ return the history of a potelet by doing a sql request
    """
    values = [potelet["id"]]
    sql = ''' SELECT * FROM history WHERE potelet_id=(?) ORDER BY date ASC'''
    cur = conn.cursor()
    cur.execute(sql, values)
    history = [dict(story) for story in cur.fetchall()]

    return history

def getClusters(conn, thres=0):
    """ return all clusters from the sqlite db
    """
    # get a list of all the cluster id
    cur = conn.cursor()

    sql = '''   SELECT COUNT(*) AS c, cluster_id
                FROM potelets
                GROUP BY cluster_id HAVING c > (?)
                ORDER BY c DESC '''
    cur.execute(sql, [thres])

    clusters_id = [cluster["cluster_id"] for cluster in cur.fetchall()]

    # an array of potelets cluster dictionnary
    potelets_by_clusters = []

    for id in clusters_id:
        potelets_sql = '''  SELECT * FROM potelets
                            WHERE cluster_id = ?
                            ORDER BY creationDate DESC '''
        cur = conn.cursor()
        cur.execute(potelets_sql, [id])
        potelets = [dict(potelet) for potelet in cur.fetchall()]

        cluster = { "id": id,
                    "incidents": potelets }

        potelets_by_clusters.append(cluster)

    return potelets_by_clusters

# ___ INSERT ENTRIES ___

def addPotelet(conn, potelet, print_details):
    # basic data
    id = potelet['id']
    status = potelet['status']
    subcat = potelet['category']['category']['nameEn']
    coordinates = json.dumps(potelet['location']['coordinates'])
    adress = (potelet['location']['address']['streetNameFr'] + ' ' +
              potelet['location']['address']['streetNumber'] + ', ' +
              potelet['location']['address']['postalCode'])
    creationDate = potelet['creationDate']
    updatedDate = potelet['updatedDate']

    if print_details:
        print('Potelet id: ' + str(id))
        print('Status: ' + status)
        print('Category: ' + subcat)
        print('Adress: ' + adress)
        print('creation date: ' + creationDate)
        print('updated date: ' + updatedDate)

    # others data (not always present)
    duplicates = potelet['duplicates']
    severalOccurrence = potelet['severalOccurrence']
    # resolved = potelet['declaredResolved']
    # third_party = potelet['thirdParty']
    # ext_id = potelet['externalId']
    # priv_location = potelet['privateLocation']

    # HARDCODE ACTORS
    organisation = potelet['responsibleOrganisation']['nameEn']
    department = potelet['responsibleDepartment']['nameEn']


    # check if potelet entry already exists
    cur = conn.cursor()
    cur.execute(''' SELECT * FROM potelets WHERE id=? ''', [potelet['id']])
    exists = cur.fetchall()

    value_list = [status, subcat, adress, coordinates, creationDate, updatedDate, duplicates, severalOccurrence, organisation, department, id]

    if exists:
        sql = ''' UPDATE potelets
                  SET status = ?, subcat = ?, adress = ?, coordinates = ?, creationDate = ?, updatedDate = ?, duplicates = ?, severalOccurrence = ?, responsibleOrganisation = ?, responsibleDepartment = ?
                  WHERE id = ?
              '''
        cur = conn.cursor()
        cur.execute(sql, value_list)
        print("potelet ["+ str(potelet['id']) +"] updated")

    else:
        sql = ''' INSERT INTO potelets(status, subcat, adress, coordinates, creationDate, updatedDate, duplicates, severalOccurrence, responsibleOrganisation, responsibleDepartment, id)
                  VALUES(?,?,?,?,?,?,?,?,?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, value_list)
        print("potelet ["+ str(potelet['id']) +"] added")


def addAttachment(conn, attachment, print_details):
    id = attachment['id']
    potelet_id = attachment['incidentId']
    date = attachment['creationDate']
    type = attachment['type']

    attachment_short = ''
    text = None;
    href = None;

    if 'text' in attachment:
        # type = 'COMMENT' or 'SYSTEM_COMMENT' or 'FILE'
        text = attachment['text']
        attachment_short += text
    if 'content' in attachment['_links']:
        # type = 'PICTURE' or 'FILE'
        href = attachment['_links']['content']['href']
        attachment_short += href

    #HARDCODE ACTORS
    actor_short = ''
    orga_name = None
    depa_name = None
    actor_type = attachment['reporter']['type']

    if actor_type == 'CITIZEN':   # it is CITIZEN, no actor id
        # CHECK: do we have others infos ??? -> NO
        actor_short = actor_type
    else:
        #-- organisation
        if 'corporation' in attachment['reporter']:
            orga = attachment['reporter']['corporation']
            orga_name = orga['nameEn']
            actor_short += orga_name
        #-- departement
        if 'team' in attachment['reporter']:
            depa = attachment['reporter']['team']
            depa_name = depa['nameEn']
            actor_short += ' → ' + depa_name

        actor_short += ' (' + actor_type + ')'
        # CHECK: other keys than 'corporation' ??? -> NO


    # check if attachment entry already exists
    cur = conn.cursor()
    cur.execute(''' SELECT * FROM attachments WHERE id=? ''', [attachment['id']])
    exists = cur.fetchall()

    value_list = [id, potelet_id, date, type, text, href, actor_type, orga_name, depa_name]

    if exists:
        pass

    else:
        sql = ''' INSERT INTO attachments(id, potelet_id, date, type, text, href, actor_type, organisation, department)
        VALUES(?,?,?,?,?,?,?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, value_list)
        print("--- attachment [" + str(attachment['id']) +"] added")

    # then print details line
    if print_details:
        print('• ' + date + ' | ' + actor_short + ': ' + attachment_short)


def addStory(conn, story, print_details):
    id = story['id']
    potelet_id = story['historizedEntityId']['id']
    date = story['historyDate']
    type = story['historyType']

    # HARDCODE ACTORS
    actor_short = ''
    orga_name = None
    depa_name = None
    actor_type = story['information']['actorType']

    if actor_type == 'CITIZEN':
        actor_short = actor_type
    else:
        #-- organisation
        if 'corporation' in story['information']:
            orga = story['information']['corporation']
            orga_name = orga['nameEn']
            actor_short += orga_name
        #-- departement
        if 'team' in story['information']:
            depa = story['information']['team']
            depa_name = depa['nameEn']
            actor_short += ' → ' + depa_name

        actor_short += ' (' + actor_type + ')'
        # CHECK: other keys than 'corporation' ??? -> no

    # check if attachment entry already exists
    cur = conn.cursor()
    cur.execute(''' SELECT * FROM history WHERE id=? ''', [story['id']])
    exists = cur.fetchall()

    value_list = [id, potelet_id, date, type, actor_type, orga_name, depa_name]

    if exists:
        pass

    else:
        sql = ''' INSERT INTO history(id, potelet_id,date, type, actor_type, organisation, department)
                  VALUES(?,?,?,?,?,?,?) '''
        cur = conn.cursor()
        cur.execute(sql, value_list)
        print("--- story [" + str(story['id']) +"] added")

    # then print details line
    if print_details:
        print('• ' + date + ' | ' + actor_short + ': ' + type)


def addAllPotelets(conn, potelets, print_details):
    i = 0
    for potelet in potelets:

        addPotelet(conn, potelet, print_details)

        if potelet['attachments']:
            if print_details: print('---[ attachments ]---')
            for attachment in potelet['attachments']:
                addAttachment(conn, attachment, print_details)

        if potelet['history']:
            if print_details: print('---[ history ]---')
            for story in potelet['history']:
                addStory(conn, story, print_details)

        # we commit the change to the db tables only at the end of every potelets to not loose too much time
        conn.commit()
        i+=1
        print("-> commited changes in db ("+ str(i) +" on "+ str(len(potelets)) +")")
        print('')

# ___ UPDATING ENTRIES ___

def getLastUpdate(conn):
    """ get the lastest updatedDate from the current db
    """
    cur = conn.cursor()
    cur.execute(''' SELECT * FROM potelets ''')
    potelets = cur.fetchall()

    updatedDates = []

    for potelet in potelets:
        date_str = potelet["updatedDate"]
        date_str = date_str.split("T")[0]
        updatedDate = datetime.strptime(date_str, '%Y-%m-%d')
        updatedDates.append(updatedDate)

    lastUpdate = max(updatedDates)
    lastUpdate_str = lastUpdate.strftime("%Y-%m-%d")

    return lastUpdate_str

def addGeoCoords(conn, potelets):
    """ insert the geojson standard coordinates in the sqlite db
    """
    for potelet in potelets:
        sql = ''' UPDATE potelets
                  SET geo_coordinates = ?
                  WHERE id = ? '''
        cur = conn.cursor()
        cur.execute(sql, (json.dumps(potelet['geo_coordinates']), potelet['id']))
    conn.commit()

def addClusterId(conn, potelets):
    """ insert the cluster ids in the sqlite db
    """
    for potelet in potelets:
        sql = ''' UPDATE potelets
                  SET cluster_id = ?
                  WHERE id = ? '''
        cur = conn.cursor()
        cur.execute(sql, (potelet['clusterId'], potelet['id']))
    conn.commit()
