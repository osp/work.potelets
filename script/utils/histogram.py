import matplotlib.pyplot as plt


def getHistogram(clusters, path):
    clusterSize = [ len(x) for x in clusters ]
    hist = {}
    for val in clusterSize:
        if val in hist:
            hist[val] +=1
        else:
            hist[val] = 1

    # print(hist)
    fig, ax = plt.subplots()

    width = 1.0                             # width of rectangle
    x = [x-(width/2) for x in hist.keys()]  # bottom left of rect
    y = hist.values()                       # height of rectangle

    ax.bar(x, y, width, color='b')
    ax.set_title('Clusters by incidents they contain')
    ax.set_ylabel('Number of clusters')
    ax.set_xlabel('Number of incidents by cluster')

    for index, value in hist.items():
        plt.text(index * width, value, "["+str(value)+"]", horizontalalignment='center')

    plt.savefig(path)
