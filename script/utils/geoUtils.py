import json
import geojson
from pyproj import CRS
from pyproj import Transformer
import random

def getTransformer():
    # EPSG:31370 that would be:
    # proj4.defs("EPSG:31370","+proj=lcc +lat_1=51.16666723333333
    # +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013
    # +y_0=5400088.438 +ellps=intl
    # +towgs84=-106.869,52.2978,-103.724,0.3366,-0.457,1.8422,-1.2747 +units=m
    # +no_defs");
    # For EPSG:4326 that would be:
    # proj4.defs("EPSG:4326","+proj=longlat +datum=WGS84 +no_defs");

    # define EPSG:31370 used by fixmystreet
    crs_31370 = CRS.from_epsg(31370)
    # define EPSG:4326 (also called WGS84), global GPS
    crs_4326 = CRS.from_epsg(4326)
    # create transformer
    transformer = Transformer.from_crs(crs_31370, crs_4326, always_xy=True)

    return transformer

def convertCoord(coordinates, transformer):
    """ convert the coordinates from EPSG:31370 to EPSG:4326
        based on a transformer pyproj object
    """
    x = coordinates["x"]
    y = coordinates["y"]
    new_x, new_y = transformer.transform(x, y)
    return [new_x, new_y]

def convertAllCoords(potelets):
    """ convert the coordinates from EPSG:31370 to EPSG:4326
        of all potelets
    """
    transformer = getTransformer()
    for potelet in potelets:
        coord = json.loads(potelet["coordinates"])
        potelet["geo_coordinates"] = convertCoord(coord, transformer)
    return potelets

def randomColor():
    r = lambda: random.randint(0,255)
    return ( '#%02X%02X%02X' % ( r(),r(),r()) )

def randomColors(list):
    return [randomColor() for x in list]

def writeGeojson(geojson_file, potelets, clusterized = False, colors = []):
    """ generate valid geojson from the potelets list and put it in a geojson file
    """
    features = []
    for potelet in potelets:

        point = geojson.Point(potelet['geo_coordinates'])
        feature = geojson.Feature(geometry=point, properties={"id": potelet["id"], "type": potelet["subcat"]})

        if clusterized:
            color = randomColor()
            feature["properties"]["marker-color"] = colors[potelet["clusterId"]]  # color format compatible with geojson.io
            feature["properties"]["cluster"] = potelet["clusterId"]               # tag the cluster if of this point

        features.append(feature)

    feature_collection = geojson.FeatureCollection(features)

    with open(geojson_file, 'w') as f:
       geojson.dump(feature_collection, f)
