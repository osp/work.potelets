import json
import geojson
import operator
import datetime

URL = 'http://fixmystreet.brussels/'

def format_date(date_str):
    # %Y-%m-%dT%H:%M:%S
    date = {}
    date_split = date_str.split("T")
    date["day"] = ".".join(date_split[0].split("-")[::-1])
    if len(date_split)==2:
        date["time"] = ":".join(date_split[1].split(":")[:2])
    return date

def format_actor(actor_type, organisation, department):
    actor = ""
    if organisation or department:
        if organisation:
            actor += organisation
        if actor_type:
            actor += " [" + actor_type + "]"
        if department:
            actor += " — " + department
    else:
        actor = actor_type

    if actor == "":
        print("ERROR: empty actor")
    return actor

def print_if_long_case(potelet):
    # interesting case?
    creationDate = datetime.datetime.strptime(potelet["creationDate"]["day"], '%d.%m.%Y')
    updatedDate = datetime.datetime.strptime(potelet["updatedDate"]["day"], '%d.%m.%Y')
    year = (updatedDate - creationDate).days
    if year >= 365:
        if len(potelet["attachments"])>4:
            print(potelet["id"], year)

def sort_attachment(potelet):
    #resort attachment in a way that for each day/actor we first have all the text,
    #then all the images
    # attachments_by_day = []
    attachments = potelet["attachments"]

    if len(attachments) > 1:
        i = 1
        while i < len(attachments):
            swap = False
            curr = attachments[i]
            prev = attachments[i-1]
            if curr["type"] == "COMMENT" and prev["type"] == "PICTURE":
                day = curr["date"]["day"]
                previous_day = prev["date"]["day"]
                actor = curr["actor"]
                previous_actor = prev["actor"]
                if day == previous_day and actor == previous_actor:
                    # swap
                    print(potelet["id"], "swap!")
                    attachments[i], attachments[i-1] = attachments[i-1], attachments[i]
                    if i > 1:
                        i -= 1
                        swap = True
            if not swap:
                i += 1

    return attachments

def detect_if_has_content(potelet):
    has_img_or_txt = False

    for attachment in potelet["attachments"]:
        type = attachment["type"]
        if type == "COMMENT" or type == "PICTURE":
            has_img_or_txt = True

    return has_img_or_txt

def detect_first_after_initial(potelet):
    if potelet["attachments"] != []:
        first_attachment = potelet["attachments"][0]
        first_actor = first_attachment["actor"]
        first_date = first_attachment["date"]
        first_posts = True;
        i = 0
        while first_posts and i < len(potelet["attachments"]):
            attachment = potelet["attachments"][i]
            first_posts = (attachment["actor"] == first_actor and attachment["date"]["day"] == first_date["day"])
            i += 1
        if not first_posts:
            attachment["firstafterinitial"] = True

def formatingPotelet(potelet):

    # loads the dumped json
    potelet["coordinates"] = json.loads(potelet["coordinates"])
    potelet["geo_coordinates"] = json.loads(potelet["geo_coordinates"])

    # format date
    potelet["updatedDate"] = format_date(potelet["updatedDate"])
    potelet["creationDate"] = format_date(potelet["creationDate"])

    # print_if_long_case(potelet)

    # add link to the original website
    potelet["url"] = URL + str(potelet["id"])
    potelet["actor"] = format_actor("", potelet["responsibleOrganisation"], potelet["responsibleDepartment"])

    # format attachment
    if potelet["attachments"] != []:

        for attachment in potelet["attachments"]:
            attachment["date"] = format_date(attachment["date"])
            attachment["actor"] = format_actor(attachment["actor_type"], attachment["organisation"], attachment["department"])

    # format history
    for story in potelet["history"]:
        story["date"] = format_date(story["date"])
        story["actor"] = format_actor(story["actor_type"], story["organisation"], story["department"])

    # add a class to the potelet without COMMNENTS or PICTURES
    potelet["img_or_txt"] = "" if detect_if_has_content(potelet) else "empty-content"

    # sort attachment with for each day first the COMMENTS then the PICTURES
    # in order to win some space while conserving chronology
    potelet["attachments"] = sort_attachment(potelet)

    # detect the first post after the "initial one"
    # when it changes either of day or of actor
    detect_first_after_initial(potelet)

    return potelet

def formatingCluster(cluster):

    # write the size
    cluster["size"] = len(cluster["incidents"])

    #street names
    cluster["streetnames"] = set()
    for p in cluster["incidents"]:
        streetname = " ".join(p["adress"].split(" ")[:-2])
        cluster["streetnames"].add(streetname)

    # creation dates
    # pas besoin de trier les dates car la requette sql le fait déjà
    cluster["dates"] = []
    for p in cluster["incidents"]:
        date = format_date(p["creationDate"])
        cluster["dates"].append(date)
        p["last_in_cluster"] = cluster["incidents"][-1]["id"]
    # add a class to the last one of every cluster
    # cluster["incidents"][-1]["last_in_cluster"] = "last-in-cluster"

    return cluster

def municipalityFilter(clusters, municipality):

    # add a municipality set to every cluster
    for cluster in clusters:
        cluster["municipality"] = {}

        for p in cluster["incidents"]:
            m = p["adress"].split(" ")[-1]

            if m in cluster["municipality"]:
                cluster["municipality"][m] += 1
            else:
                cluster["municipality"][m] = 1

        # municipality of the cluster is the first one with the more incidents
        cluster["biggest_municipality"] = max(cluster["municipality"].items(), key=operator.itemgetter(1))[0]

    # filter out of the list the cluster that don't have the choosen munucipality
    filtered_clusters = [cluster for cluster in clusters if str(municipality) == cluster["biggest_municipality"]]

    return filtered_clusters

def addGeojsonToClusters(clusters):
    """ take a big geojson and put the date concerning one cluster
        in the cluters list
    """
    for cluster in clusters:

        features = []
        for potelet in cluster["incidents"]:

            point = geojson.Point(potelet['geo_coordinates'])
            feature = geojson.Feature(geometry=point, properties={"id": potelet["id"], "type": potelet["subcat"]})
            features.append(feature)

        cluster["geojson"] = dict(geojson.FeatureCollection(features))

    return clusters
