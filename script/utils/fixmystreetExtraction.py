import json
import requests

url = 'http://fixmystreet.brussels/api/'
urlcat = 'categories'
urlinc = 'incidents'
headers = {'Accept': 'application/hal+json'}

originDate = '2000-01-01'

mobilierurbain_catid = 1007
potelet_catid = 2030

# ratio for the number of items got by request
itemsbypages = 24

# ___ API EXTRACTING ___

def requestPoteletCat():
    """ get the potelet category json object with their subcategories
    """
    potelet_cat = {}
    r = requests.get(url+urlcat, headers = headers)
    categories = r.json()['response']['categories']
    for category in categories:
        if category['id'] == mobilierurbain_catid:
            for subcategory in category['subCategories']:
                if subcategory['id'] == potelet_catid:
                    potelet_cat = subcategory
    return potelet_cat

def requestAmountOfIncidents(since=originDate, category=''):
    """ get the number of incident for a category, and the number of pages (requests) needed to get them
        if no category is precised, get the total of incident
    """
    params = { 'startDate': since,
               'size': itemsbypages }
    if category:
        params['category'] = category

    r = requests.get(url+urlinc, headers = headers, params = params)
    number = r.json()['page']['totalElements']
    pages = r.json()['page']['totalPages']
    return number, pages

def requestTotalNumberOfPotelets():
    """ get the total number of potelets incident on the fixmystreet website
    """
    potelet_cat = requestPoteletCat()
    total_number_of_potelet = 0
    for potelet_subcat in potelet_cat['subCategories']:
        total_number_of_potelet += requestAmountOfIncidents(category=potelet_subcat['id'])[0]
    return total_number_of_potelet

def requestPotelets(since=originDate):
    """ make the request to the fixmystreet API and return a potelets list
    """
    potelets = []
    potelet_cat = requestPoteletCat()

    # we have to make a different request for every subcat of potelet
    for potelet_subcat in potelet_cat['subCategories']:
        number, pages = requestAmountOfIncidents(since, potelet_subcat['id'])
        print("* " + potelet_subcat['nameEn'] + ' (' + str(number) + ') - ' + str(pages) + ' pages of ' + str(itemsbypages))
        for i in range(pages):
            # we have to make one request by pages
            params = { 'startDate': since,
                       'category': potelet_subcat['id'],
                       'size': itemsbypages,
                       'page': i }
            print('pages: ' + str(i+1) + '/' + str(pages) + ' ...')
            r = requests.get(url+urlinc, headers = headers, params = params)
            potelets += r.json()['_embedded']['response']
        print('')
    return potelets

def requestAttachments(potelet):
    """ get the attachments list (COMMENTS and PICTURES) of a potelet
    """
    url_attachments = potelet['_links']['attachments']['href']
    attachments = requests.get(url_attachments, headers = headers).json()
    #sometimes it's just an empty dict, we transform it into a list...
    attachments = attachments['response'] if attachments else []
    return attachments

def requestHistory(potelet):
    """ get the history list (changes of status) of a potelet with its id
    """
    url_history = potelet['_links']['history']['href']
    history = requests.get(url_history, headers = headers).json()
    history = history['response'] if history else []
    return history
