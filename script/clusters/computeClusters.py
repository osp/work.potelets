import numpy as np

# ------------------------------------------------------------------------------
# Find clusters and add a property to identify them ----------------------------
def computeClusters(data, clusterMaxDistance):

    # Initialization -----------------------------------------------------------
    coords = np.array([ np.array(x["euc_coordinates"]) for x in data ])

    # Get neighbour adjacancy list ---------------------------------------------
    adjList = getAdjListOfNeighbours(coords, clusterMaxDistance)

    # Get clusters -------------------------------------------------------------
    clustered = set()
    clusterId = 0
    clusters = []
    for i in range(len(data)):
        if( not (i in clustered) ):
            cluster = findCluster(i, adjList)
            clusters.append(cluster)
            for element in cluster:
                data[element]["clusterId"] = clusterId
                clustered.add(element)  # set a clustered so that we do not redo the computations of cluster again and again
            clusterId +=1

    return clusters


# ------------------------------------------------------------------------------
# Get adjacency list of a coord array where p1 is adjacent to p2 if
# distance(p1, p2) < clusterMaxDistance
def getAdjListOfNeighbours(coords, clusterMaxDistance):
    distMatrix = np.linalg.norm(coords - coords[:,None], axis = -1)
    adjList = []
    for i in range(len(coords)):
        neibours = []
        for j in range(len(coords)):
            if distMatrix[i, j] < clusterMaxDistance:
                neibours.append(j)
        adjList.append(neibours)
    return adjList


# ------------------------------------------------------------------------------
# Get connected componant of a point i -----------------------------------------
def findCluster(i, adjList):      # This is a depth-first search algorithm
    cluster = {i}
    queue = [i]
    while len(queue) > 0:
        thisNode = queue.pop()
        nodes = adjList[thisNode]
        for node in nodes:
            if( not (node in cluster) ):
                cluster.add(node)
                queue.append(node)
    return cluster
























"""
def LOF(points, k):
    n = len(points)
    # (1) -----------------------------------------------------------------------------
    # Find distance matrix:                                               (n, n)-matrix
    distMat = np.linalg.norm(points - points[:,None], axis = -1)

    # (2) -----------------------------------------------------------------------------
    # Find k nearest points id matrix sorted by order:                    (n, k)-matrix
    kFirstNeib = np.argsort(distMat, axis=-1)[:,1:k+1]
    # Find k nearest point-distances
    kFistDist = np.take_along_axis(distMat, kFirstNeib, axis=-1)

    # (3) -----------------------------------------------------------------------------
    # Find k-distances                                                          n-array
    kD = kFistDist[:,k-1]

    # Find reachability distances                                        (n, k')-matrix
    RD = np.maximum(np.take(kD, kFirstNeib), kFistDist)


    # (4) -----------------------------------------------------------------------------
    # Find local reachability distances                                         n-array
    RDmean = np.mean(RD, 1)
    LRD = np.vectorize(lambda x: np.inf if x == 0 else 1/x)(RDmean)
    print(kFirstNeib[0])
    print(kFistDist[0])
    print(kD[0])
    print(RD[0])
    print(LRD[0])

    # (5) -----------------------------------------------------------------------------
    # Find local outlier factor                                                 n-array
    lof = np.mean(np.take(LRD, kFirstNeib),1)/LRD
    print(lof[0])

    return lof
"""
