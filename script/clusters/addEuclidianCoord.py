import math
import json


# ------------------------------------------------------------------------------
# Distance between two coordiantes in [lng, lat] -------------------------------
def haversine(coord1, coord2):
    [lng1, lat1] = coord1
    [lng2, lat2] = coord2
    EARTH_DIAMETER = 12742e3
    phi1 = (lat1 * math.pi) / 180
    phi2 = (lat2 * math.pi) / 180
    dphi = phi2 - phi1
    dlambda = ((lng2 - lng1) * math.pi) / 180
    return (
        EARTH_DIAMETER *
        math.asin(
            math.sqrt(
                math.sin(dphi / 2) ** 2 +
                math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2
            )
        )
    )


# ------------------------------------------------------------------------------
# Get scale between [lng, lat] => [x, y] in meters around center (Euclidean Approximation)
def getScale(center):
    epsilon = 0.0001
    centerX = [center[0] + epsilon, center[1]]
    centerY = [center[0], center[1] + epsilon]
    dx = haversine(center, centerX) / epsilon
    dy = haversine(center, centerY) / epsilon
    return dx, dy

# ------------------------------------------------------------------------------
# Add Euclidan coord system to a geojson FeatureCollection ---------------------
def addEuclidianCoord(potelets, center):
    dx, dy = getScale(center)
    for potelet in potelets:
        coord = potelet["geo_coordinates"]
        euclideanCoord = [(coord[0] - center[0]) * dx, (coord[1] - center[1]) * dy]
        potelet["euc_coordinates"] = euclideanCoord
    return potelets
