import json


def readJson(path):
    strData = open(path, "r")
    data = strData.read()
    strData.close()
    return json.loads(data)

def writeJson(path, obj):
    f = open(path, "w")
    f.write(json.dumps(obj))
    f.close()
