""" This script get all the potelets incidents back from the database
    And add a new coordinate that is the standard geojson latitude longitude coordinate
    It writes them in the database
    And also generate a geojson file with all the potelets incidents
"""

from utils.dbInteraction import createConnection, getPotelets, addGeoCoords

from utils.geoUtils import convertAllCoords, writeGeojson

DB_PATH = '../data/potelets.db'
GEOJSON_PATH = '../data/potelets.geojson'

# ___ MAIN ___

if __name__ == '__main__':

    print('___ Write standard geojson coordinates ___')

    print('1. Getting back the potelets from the db ' + DB_PATH)
    conn = createConnection(DB_PATH)
    potelets = getPotelets(conn)

    print('2. Converting coordinates (epsg:31370 to epsg:4326)')
    potelets = convertAllCoords(potelets)

    print('3. Write standard geojson coordinates in ' + DB_PATH)
    addGeoCoords(conn, potelets)

    print('4. Write geojson in ' + GEOJSON_PATH)
    writeGeojson(GEOJSON_PATH, potelets)
    print('-- writed geojson file of ' + str(len(potelets)) + ' point')
