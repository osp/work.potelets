""" Clusterisation Code by Matvei Tsishyn (matvei.tsishyn@gmail.com)
    edited by me to make it work with the sqlite db and the rest of the workflow
"""

import json

from utils.dbInteraction import createConnection, getPotelets, addClusterId
from utils.geoUtils import writeGeojson, randomColors

from clusters.addEuclidianCoord import addEuclidianCoord
from clusters.computeClusters import computeClusters

DB_PATH = '../data/potelets.db'
GEOJSON_OUT_PATH = "../data/potelets_clustered.geojson"

CENTER_BXL = [ 4.35264587402343, 50.84741037657682 ]
CLUSTER_MAX_DISTANCE = 30



if __name__ == '__main__':

    print('___ Clusterize ___')
    print("")

    # Read Data
    print('1. Getting back the potelets from the db ' + DB_PATH)
    conn = createConnection(DB_PATH)
    potelets = getPotelets(conn)
    for potelet in potelets:    #json loads the standard lat and long coordinates
        potelet['geo_coordinates'] = json.loads(potelet['geo_coordinates'])
    print("-- Data contains ", len(potelets), " items")
    print("")

    # Add Euclidean approximations coordinate system to all potelets in euc_coordinates => [x, y]'
    print("2. Adding euclidean approximations around center point ", CENTER_BXL)
    addEuclidianCoord(potelets, CENTER_BXL)
    print("")

    # Compute Clusters where every cluster is a set of index of the data list
    print("3. Find clusters where each point is separated by maximum ", CLUSTER_MAX_DISTANCE, "meters from the rest of the cluster points")
    clusters = computeClusters(potelets, CLUSTER_MAX_DISTANCE)
    print("-- There is ", len(clusters), "clusters")
    print("")

    # Save file with clusters
    # Tag a cluster id for each feature as 'feature.properties.cluster'
    # Add a color so we can visualize in on geojson.io as 'features.properties["marker-color"] => random html color in in hexadecimal'
    print("4. Writing cluster id in geojson")
    colors = randomColors(clusters)
    writeGeojson(GEOJSON_OUT_PATH, potelets, True, colors)
    print("-- Clustedred data saved in ", GEOJSON_OUT_PATH)
    print("")

    print("5. Writing cluster id in db")
    addClusterId(conn, potelets)
    print("-- Clustedred data saved in ", DB_PATH)
    print("")
