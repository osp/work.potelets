import matplotlib.pyplot as plt
import numpy as np

from utils.dbInteraction import (
    createConnection,
    getClusters,
)
from utils.formatingUtils import (
    municipalityFilter,
)

MUNICIPALITIES = {
    1000: "Bruxelles-Ville",
    1020: "Laeken",
    1030: "Schaerbeek",
    1040: "Etterbeek",
    1050: "Ixelles",
    1060: "Saint-Gilles",
    1070: "Anderlecht",
    1090: "Jette",
    1080: "Molenbeek-Saint-Jean",
    1081: "Koekelberg",
    1082: "Berchem-Sainte-Agathe",
    1083: "Ganshoren",
    1140: "Evere",
    1150: "Woluwe-Saint-Pierre",
    1160: "Auderghem",
    1170: "Watermael-Boitsfort",
    1180: "Uccle",
    1190: "Forest",
    1200: "Woluwe-Saint-Lambert",
    1210: "Saint-Josse-ten-Noode"
}

DB_PATH = '../data/potelets.db'
HISTO_CBI_PATH = "../data/clustersbyincidents.png"
HISTO_CBC_PATH = "../data/clustersbycommunes.png"
HISTO_IBC_PATH = "../data/incidentsbycommunes.png"

def clusterByIncidentHist(data, path):

    fig, ax = plt.subplots()

    width = 0.8                             # width of rectangle
    # x = np.arange(len(data.keys()))        # an array of equal interval
    x = data.keys()
    y = data.values()                       # height of rectangle

    ax.bar(x, y, width, color='b')
    ax.set_title('Clusters by incidents they contain')
    ax.set_ylabel('Number of cluster')
    ax.set_xlabel('Number of incidents by cluster')

    for key, value in data.items():
        plt.text(key, value, str(value), rotation = 45, ha='center')

    plt.tight_layout()
    plt.savefig(path)

def clusterByCommuneHist(data, path):

    fig, ax = plt.subplots()

    width = 0.8                             # width of rectangle
    x = np.arange(len(data.keys()))         # an array of equal interval
    # x = [i-(width/2) for i in x]          # bottom left of rect
    y = data.values()                       # height of rectangle

    ax.bar(x, y, width, color='b')
    ax.set_title('Clusters by communes')
    ax.set_ylabel('Number of clusters')
    # ax.set_xlabel('Number of incidents by cluster')

    ax.set_xticks(x)
    ax.set_xticklabels(data.keys(), rotation = 45, ha="right")

    plt.tight_layout()
    plt.savefig(path)

def incidentByCommuneHist(data, path):

    fig, ax = plt.subplots()

    width = 0.8                             # width of rectangle
    x = np.arange(len(data.keys()))         # an array of equal interval
    y = data.values()                       # height of rectangle

    ax.bar(x, y, width, color='b')
    ax.set_title('Incidents by communes')
    ax.set_ylabel('Number of incidents')
    # ax.set_xlabel('Number of incidents by cluster')

    ax.set_xticks(x)
    ax.set_xticklabels(data.keys(), rotation = 45, ha="right")

    plt.tight_layout()
    plt.savefig(path)


if __name__ == '__main__':

    print('1. Getting back the potelets from the db ' + DB_PATH)
    conn = createConnection(DB_PATH)
    clusters = getClusters(conn)
    print("-- Data contains", len(clusters), "clusters")

    print("2. Generating", HISTO_CBI_PATH ,"Histogram")
    clusterSize = [ len(x["incidents"]) for x in clusters ]
    cluster_by_incident = {}
    for val in clusterSize:
        if val in cluster_by_incident:
            cluster_by_incident[val] +=1
        else:
            cluster_by_incident[val] = 1
    # max = max(cluster_by_incident.keys())
    # for i in range(max):
    #     if not i in cluster_by_incident:
    #         cluster_by_incident[i] = 0
    clusterByIncidentHist(cluster_by_incident, HISTO_CBI_PATH)
    print("-- Histogram saved in ", HISTO_CBI_PATH)

    print('3. Filtering Clusters by municipalities')
    clusters_by_commune = {}
    incidents_by_commune = {}
    for code, name in MUNICIPALITIES.items():
        clusters_in_muni = municipalityFilter(clusters, code)
        clusters_by_commune[name] = len(clusters_in_muni)
        incidents_by_commune[name] = 0
        for c in clusters_in_muni:
            incidents_by_commune[name] += len(c['incidents'])

    # print(clusters_by_commune)
    # print(incidents_by_commune)

    # print("4. Generating", HISTO_CBC_PATH ,"Histogram")
    # clusterByCommuneHist(clusters_by_commune, HISTO_CBC_PATH)
    # print("-- Histogram saved in ", HISTO_CBC_PATH)

    print("5. Generating", HISTO_IBC_PATH ,"Histogram")
    incidentByCommuneHist(incidents_by_commune, HISTO_IBC_PATH)
    print("-- Histogram saved in ", HISTO_IBC_PATH)
