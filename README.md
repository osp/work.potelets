# Potelet / Paaltje / Bollard

Creating a printed object of the potelets incidents of the [fixmystreet.brussels](http://fixmystreet.brussels) app, for [MAMA](http://mama.brussels).

Information on the API can be found here:
  * https://data.gov.be/fr/dataset/01593a26-ed57-498e-bec0-13011a75a773
  * https://api.brussels/store/apis/info?name=fixmystreet&version=1.0.0&provider=admin

## Extraction

```
cd script
python3 extract.py
```

This will extract all the potelets incidents by using the fixmystreet API and put them in a local sqlite db ```data/potelets.db```.
If the db already exist it will search for updates from the fixmystreet API and write them in the db.

Note:
  * extracting all the potelets and write them in the db with the python script **takes approximatelly 3h**.
  * if an incidents **has changed of category**, the update script is not able to see it (as the responses of the API only gives infos on the ones that are still in the category - potelets)

To look at db in the command line:
  * ```sudo apt install sqlite3```
  * ```cd data```
  * ```sqlite3```
  * ```.open potelets.db```
  * ```.header on```
  * ```.mode column```
  * ```select * from potelets;```

Or use [sqlite DB browser](https://sqlitebrowser.org/).

## Clusterization

```
cd script
python3 geo.py
python3 cluster.py
```

The geo script read all incidents from the sql database and add a new coordinate to them that is the standard geojson latitude longitude (as fixmystreet uses another one: EPSG:31370).
It also generates a geojson file with all the potelets incidents that you can easily plot, on [geojson.io](https://geojson.io) for example. You have to run this script first in order to run cluster.py.

The cluster script ead all incidents from the sql database, and groups incidents that are nearby each others into clusters.
It generates a new geojson file with a different marker color for every cluster.
It also write the cluster id into the sql database.

## Layout

```
cd script
python3 layout.py
```

This will generate the HTML file, by filling the ```layout/template/template.html``` with the data from the ```data/potelets.db```.
The layout uses **paged.js** for pagination, which need to be on a server in order to work.
Open the server in a web browser and you can get your .pdf from here.
Note that you have to rerun this python script every time you make a change to the template, then the server should update automatically.

To avoid ultra big .pdf we can resample the images down with ```resample-pdf.sh``` with this command
```
./resample-pdf.sh [dpi] [$pdffile]
```
