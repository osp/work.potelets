# Potelet / Paaltje / Bollard - Documentation

## data structures

There is two subcategories of potelet: *damaged* and *missing*.
There is respectively *4515* and *1833* items in each.
For an idea the whole website contains *129781* items.
<!-- We can only make a request by category if we send a subcategory with no subcategory.
So we have to make a different request for every subcategory of potelet. -->
<!-- We can get all the potelet in every of those category with one call. -->

Every incidents contains mainly an adress, a location in x,y coordinate, a status (*CREATED*, *PROCESSING* and *CLOSED*), a creation and last updated date, and some responsible organisation/department linked to it.


### attachments

For every incident there is an **attachments** thread/section, that contains the **pictures** and **comments**.
Those are like a thread of different actors interacting by posting images or texts, every attachment has a date of publication.

Types of attachment entries:
COMMENT, DISMISSAL_COMMENT, FILE, MERGE_COMMENT, PICTURE, REOPEN_COMMENT, SYSTEM_COMMENT.
<!-- SELECT type FROM attachments GROUP BY type; -->


### history

For every potelet there is an **history** thread/section, that contains its changes of status.
The status changes can give us informations about the progression of the incidents on a timeline.

Types of history entries:
INCIDENT_ACCEPTED, INCIDENT_ASKED_REOPENING, INCIDENT_CLOSED, INCIDENT_CREATED, INCIDENT_DISMISSED, INCIDENT_DISMISSED_BY_MERGE, INCIDENT_GRANTEE_ASSIGNED, INCIDENT_MERGED, INCIDENT_PRIVATIZED, INCIDENT_PUBLISHED, INCIDENT_REOPENED, INCIDENT_UPDATE_PUBLISHED.
<!-- SELECT type FROM history GROUP BY type; -->

### actors

For every entries in the attachment or history section, there is an **actor**. Those actors are of **types**:
  * *CITIZEN* (users with their private data hidden by the API),
  * *PROFESSIONAL* (a *organisation* or a *departement*)
  * *SYSTEM* (the admins: "Région Bruxelles-Capitale (Bruxelles Mobilité)", "Bruxelles-Propreté" (?), "Sibelga Eclairage" (?))
  * *UNKNOWN* (rare and only for early entries...)

For *PROFESSIONAL*, there are two subcategories of actors: *organisation* and *department*.
Every *department* is linked to a certain *organisation*. So *organisation* and *departement* have **separate id systems** and are from different tables.
<!-- in fact organisation and departement must be two different tables.
https://fixmystreet.brussels/api/incidents/267692/ (resp org id = 4)
https://fixmystreet.brussels/api/incidents/266302/ (resp dep id = 4) -->

Attachments entries, when the type of the actor is *PROFESSIONAL*, are always marked has made by an *organisation*, whithout precision of the *departement*, and accompagnied with the *organisation* id.
There is no *SYSTEM* type entries in attachments.
<!-- SELECT * FROM attachments WHERE department is not null;-->
<!-- SELECT * FROM attachments GROUP BY actor_type; -->

History entries, when the type of the actor is *PROFESSIONAL*, are marked has made by both an *organisation* and a *departement*, but no id is precised...
For the *SYSTEM* ones, then only an *organisation* (and no *department*) is precised.
<!-- SELECT * FROM history WHERE actor_type="SYSTEM" and department is not null; -->

Two actors are also automatically assigned at the creation of an incident (one *organisation* and one *department*), even before it is marked has *PROCESSING* (when it is still marked has *CREATED*). It may be associated autimatically according the postal code?
Sometimes the responsible *organisation* is the *SYSTEM* one ("Région Bruxelles-Capitale (Bruxelles Mobilité)"), in this case the *department* may vary though.
We also have separate contact informations for the organisation and the department here (not yet extracted from the db).
<!-- select * from potelets where responsibleOrganisation="Région Bruxelles-Capitale (Bruxelles Mobilité)" GROUP BY responsibleDepartment; -->


### schemes

This makes a structure of 4 tables plus an image folder.

![alt text](datastructure.png "Title")

A special case is the **duplicate** [à completer]

## stats on images

Most of potelet (around 77%) have 0 or 1 image.
Some (around 22%) have between 2 and 6 images.
Then only 0.003% have 7 images or more.

```
Total number of Potelet: 6349
Total number of Images: 6727
Average number of images by potelet: 1.0595369349503858
```

```
0: 1867
1: 3079
2: 930
3: 295
4: 96
5: 41
6: 20
7: 9
8: 5
10: 2
11: 1
12: 2
13: 1
23: 1
```

## classification and ordering possibilities

### classification
* separate by **categories**: *damaged* and *missing*.
* separate by **by actors**: *responsible organisation* and/or *responsible departement* in charge of the incident.
* separate by **status**: *CREATED*, *PROCESSING*, *CLOSED*
* separate by **location**: *municipality*.
* separate by **type of location**: analysis on the picture and/or location to detect if it's a crossection, a small street, etc.

### filtering
* only those with marked with the **severalOccurrence** or **duplicates** special tag. Though there seems to be no way to access the one with which there are duplicate.
* only those at specific **urbanistic contexts** (need to be computed in some ways?)
* only those with certain attachment type ("REOPEN_COMMENT" for example)
* only those with certain history type ("INCIDENT_ASKED_REOPENING" for example)
* only those with a certain number of **attachments**, or number of comments?.
* only those with at least an **attachment** from a *PROFESSIONAL*, meaning: a start of response/conversation.


### ordering
* order by **creation date**
* order by **last updated date**
* order like **a walk** that **minimise** the change of lo for location and/or time.

### WHICH ONES INTEREST US?

1. The one with at the same time images and text.
2. To compare the one in a same cluster/location (look where there is a condensation of incidents?)

## other notes

For every incident there is already an automaticaly generated .pdf everyone can download on the website.
We can get it with the API: ```https://fixmystreet.brussels/api/incidents/{id}/pdf?lang={lang}&addressLang={addressLang}&type={type}```

A data that is important but doesn't appear in the individual information about potelet incidents is **the macro repartition** of the incident on a map with it's *cluster* and *hole*.
This could be explicitly represented in the final object if there is one section by municipality and some are nearly empty while others are big.
