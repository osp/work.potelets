#! /bin/bash

dpi=$1;
pdffile=$2;

gs \
  -o "${pdffile%.pdf}-resampled.pdf" \
  -sDEVICE=pdfwrite \
  -dDownsampleColorImages=true \
  -dDownsampleGrayImages=true \
  -dDownsampleMonoImages=true \
  -dColorImageResolution=$dpi \
  -dGrayImageResolution=$dpi \
  -dMonoImageResolution=$dpi \
  -dColorImageDownsampleThreshold=1.0 \
  -dGrayImageDownsampleThreshold=1.0 \
  -dMonoImageDownsampleThreshold=1.0 \
  -dAutoRotatePages=/None \
   "${pdffile}"
