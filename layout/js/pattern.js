
const PATTERN_W = 43;
const PATTERN_H = 34;

const BROKEN_TRES = 0.2;
const STREET_TRES = 0.5;

function add_potelet(){
  let p;
  if (Math.random() < BROKEN_TRES){
    if (Math.random() < 0.5){
      p = "<span class='damaged'>¡</span>";
    }
    else{
      p = "<span class='missing'>¡</span>";
    }
  }
  else{
    p = "¡";
  }
  return p;
}

function init_patterns(){
  console.log("generating patterns pages");

  patterns = document.getElementsByClassName('pattern');

  for (let pattern of patterns) {

    let adding = "_";

    for (var i = 0; i < PATTERN_H; i++) {
      for (var j = 0; j < PATTERN_W; j++) {
        if (adding == "¡"){
          pattern.innerHTML += add_potelet();
          adding = "_";
        }
        else if (adding == "_"){
          pattern.innerHTML += "_";
          if (Math.random() > STREET_TRES){
            adding = "¡";
          }
        }
      }
      pattern.innerHTML += "<br/>";
    }
  }
}
