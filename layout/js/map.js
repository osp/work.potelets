
// Zoom: open street map cannot give us zoom over 18
// so we have to set up a MaxNativeZoom (above which it interpolate the tiles)
// also the fitBounds only set the custom zoom by steps
// (padding below 30 doesn't affect most map and over 30 it make all zoom dezoom by one step that is too big) ...

function init_map(mapid, geojsonObject){

  // create map object
  var mymap = L.map(mapid).setView([50.84673, 4.35247], 12);

  // add OpenStreetMap belgium tile
  L.tileLayer('https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png', {
    attribution:
        '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors' +
        ', Tiles courtesy of <a href="https://geo6.be/">GEO-6</a>',
    maxZoom: 19,
    maxNativeZoom: 18
  }).addTo(mymap);

  var geojsonLayer = L.geoJSON(
    geojsonObject,{
      pointToLayer: function (feature, latlng) {
        // should return an ILayer object for every point/feature

        // choose style
        // let symbol;
        // markerStyle = {}
        // switch (feature.properties.type) {
        //     case 'damaged':
        //       symbol = "!";
        //       break;
        //     case 'missing':
        //       symbol = "?";
        //       break;
        // }

        let marker = L.marker(latlng, {
            icon: new L.DivIcon({
              className: 'marker',
              html: '<div class="marker-box"><div class="flex-label"><div class="'+feature.properties.type+'">'+feature.properties.id+"</div></div></div>"
            })
        });

        return marker;
      }
  });

  var markers = L.markerClusterGroup({
    // define an icon create function to create the multi icon
     iconCreateFunction: function(cluster) {
        let clustered_markers = cluster.getAllChildMarkers();
        // clustered_markers = clustered_markers.sort(function(a,b){
        //   return a.feature.properties.id < b.feature.properties.id;
        // });
        clustered_markers.reverse();
        let clustered_id = clustered_markers.map( e => '<div class="'+e.feature.properties.type+'">'+e.feature.properties.id+"</div>" );
        let icon_html = '<div class="marker-box"><div class="flex-label">' + clustered_id.join("") + '</div></div>';
        return L.divIcon({ html: icon_html });
     },
     maxClusterRadius: 20
  });

  markers.addLayer(geojsonLayer);
  mymap.addLayer(markers);

  // geojsonLayer.addTo(mymap);
  mymap.fitBounds(geojsonLayer.getBounds(),{
    padding: L.point([70, 70])
    // maxZoom
  });

  console.log(mapid + " initiated");
}

function init_all_maps(){
  console.log("initializing the maps");

  // getting global dumped geojson from python
  let globalGeojson = window.geojson;

  // all the element with map class
  maps = document.getElementsByClassName('map');

  for (let map of maps) {
    // get the id of the cluster through the map id
    let mapid = map.id;
    let clusterid = mapid.split("-")[1]
    // get the geojson of the cluster through globalGeojson dict
    let clusterGeojson = globalGeojson[clusterid];
    // init this map
    init_map(mapid, clusterGeojson);
  }

}

function init_cover_map(){

  // getting global dumped geojson from python
  let globalGeojson = window.geojson;
  // console.log(globalGeojson);

  // create map object
  var mymap = L.map("cover-map").setView([50.84673, 4.35247], 12);

  // add OpenStreetMap belgium tile
  L.tileLayer('https://tile.openstreetmap.be/osmbe/{z}/{x}/{y}.png', {
    attribution:
        '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors' +
        ', Tiles courtesy of <a href="https://geo6.be/">GEO-6</a>',
    maxZoom: 19,
    maxNativeZoom: 18,
    opacity: 0
  }).addTo(mymap);

  mymap._handlers.forEach(function(handler) {
    handler.disable();

  });

  let geojsonObject = {
    type: "FeatureCollection",
    features: []
  };
  for (const id in globalGeojson) {
    geojsonObject["features"] = geojsonObject["features"].concat(globalGeojson[id]["features"]);
  }

  var geojsonLayer = L.geoJSON(
    geojsonObject,{
      pointToLayer: function (feature, latlng) {
        // should return an ILayer object for every point/feature

        // choose style
        let symbol;
        markerStyle = {}
        switch (feature.properties.type) {
            case 'damaged':
              symbol = "!";
              break;
            case 'missing':
              symbol = "?";
              break;
        }
        // let marker = L.circleMarker(latlng, markerStyle);
        let marker = L.marker(latlng, {
            icon: new L.DivIcon({
              className: 'marker',
              html: "<span>"+symbol+"</span>"
            })
        });
        return marker;
      }
  });

  geojsonLayer.addTo(mymap);
  mymap.fitBounds(geojsonLayer.getBounds(),{
    padding: L.point([50, 50])
    // maxZoom
  });

  console.log("cover map initiated");

}
